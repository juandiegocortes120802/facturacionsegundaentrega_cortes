INSERT INTO Cliente(Nombre, Apellido, RFC) values
('Jose', 'Perez', 'PERJ123456'),
('Azucena', 'García', 'GARA456789'),
('Serafin' , 'Lopez', 'LOPS567890'),
('Juan Diego', 'Cortes', 'COFJ020812');


INSERT INTO Productos(Codigo_Id, Stock, Nombre, Precio_Unitario) values
('PW1234', 1, 'PC Windows 10', 3000.99),
('XO1234', 4, 'Xbox One', 8500.50),
('AS1234', 3, 'Audifonos Sony', 300.95),
('TG1234', 1, 'Teclado Gamer', 300.0099);

INSERT INTO Venta(Id, Cliente, Cantidad, Producto, Total) values
('V123456', 'COFJ020812', 4, 'Audifonos Sony', 300.99),
('V246810', 'COFJ020812', 3, 'Lentes de Sol', 200);