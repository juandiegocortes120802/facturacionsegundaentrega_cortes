package com.example.FacturacionSegundaEntrega.Cortes;

import java.util.Objects;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;


// Se declara la entidad persistente
// Se crea la tabla llamada cliente y se determina una columna con cada uno de los
// factores que lo componen

@Entity
@Table(name = "Cliente")
public class Cliente {
	
	public Cliente() {
		super();
	}
	
	@Id
	@Column(name = "RFC")
	private String rfc;
	
	@Column(name = "Nombre")
	private String nombre;

	@Column(name = "Apellido")
	private String apellido;

	
	
	
	// Se crea el contructor vacio y los getters y setters que son obligatorios
	// Tambien se agregan mas metodos, esos son opcionales dependiendo la aplicacion.
	
	


	public Cliente(String nombre, String apellido, String rfc) {
		this.nombre = nombre;
		this.apellido = apellido;
		this.rfc = rfc;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getApellido() {
		return apellido;
	}


	public void setApellido(String apellido) {
		this.apellido = apellido;
	}


	public String getRfc() {
		return rfc;
	}


	public void setRfc(String rfc) {
		this.rfc = rfc;
	}


	
	
	@Override
	public String toString() {
		return "Cliente [nombre= " + nombre + ", apellido= " + apellido + ", rfc= " + rfc + "]";
	}


	@Override
	public int hashCode() {
		return Objects.hash(apellido, nombre, rfc);
	}


	


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cliente other = (Cliente) obj;
		return Objects.equals(apellido, other.apellido) && Objects.equals(nombre, other.nombre)
				&& Objects.equals(rfc, other.rfc);
	}
	
	
}
