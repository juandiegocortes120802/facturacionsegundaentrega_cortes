package com.example.FacturacionSegundaEntrega.Cortes;

import java.util.Objects;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

//Se declara la entidad persistente
//Se crea la tabla llamada Productos y se determina una columna con cada uno de los
//factores que lo componen

@Entity
@Table(name = "Productos")
public class Productos {
	
	public Productos() {
		super();
	}
	
	@Id
	@Column(name = "Codigo_Id")
	private String id;
	
	@Column(name = "Stock")
	private int stock;
	
	@Column(name = "Nombre")
	private String nombre;
	
	@Column(name = "Precio_Unitario")
	private double precio;

	
	// Se crea el contructor vacio y los getters y setters que son obligatorios
	// Tambien se agregan mas metodos, esos son opcionales dependiendo la aplicacion.
	
	
	


	public Productos(String id, String nombre, double precio, int stock) {
		
		this.id = id;
		this.nombre = nombre;
		this.precio = precio;
		this.stock = stock;
	}


	public void setId(String id) {
		this.id = id;
	}
	
	
	public String getId() {
		return id;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public double getPrecio() {
		return precio;
	}


	public void setPrecio(double precio) {

		this.precio = precio;
	}
	
	
	public int getStock() {
		return stock;
	}


	public void setStock(int stock) {
		this.stock = stock;
	}


	@Override
	public String toString() {
		return "Productos [id= " + id + ", nombre= " + nombre + ", stock= " + stock +", precio unitario= " + precio + "]";
	}


	@Override
	public int hashCode() {
		return Objects.hash(nombre, precio, stock);
	}



	
	

	
	
}
