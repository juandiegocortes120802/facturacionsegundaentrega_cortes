package com.example.FacturacionSegundaEntrega.Cortes.Controllers;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.FacturacionSegundaEntrega.Cortes.Productos;
import com.example.FacturacionSegundaEntrega.Cortes.Venta;
import com.example.FacturacionSegundaEntrega.Cortes.Services.VentaService;







@RestController	
@RequestMapping("/ventas")	
public class VentasController {
	
	@Autowired
	private VentaService ventasService;
	
	
	/* Postear un Nuevo Producto, los datos son recibidos en formato Json con la siguiente estructura:
	 
	   	{
    		"cliente": "COFJ020812",
    		"cantidad": 4,
    		"producto": "PC",
    		"total": 300.99
		}
	*/
	
	// Crear
	@PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE})      
	public ResponseEntity<?> guardarVenta(@RequestBody Venta venta) {
	

		try {
			Venta ventaGuardada = ventasService.registrarVenta(venta);
			return ResponseEntity.created(URI.create("")).body(ventaGuardada);
		} 
		catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.internalServerError().body(null);
		}
	}
	
	
	// Obtener
	// IMPRIME TODAS LA VENTAS REGISTRADAS
	// http://localhost:8090/ventas/
	@GetMapping(value = "/", produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<List<Venta>> getVentas() {
		
		return new ResponseEntity<>(this.ventasService.findAll(), HttpStatus.OK);
	}
	
	// imprime una en especifico con su id
	// http://localhost:8090/ventas/V123456
	@GetMapping(value = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<?> getVentaById(@PathVariable(name = "id") String id) {
		
		Optional<Venta> venta = ventasService.buscarVentaById(id);
		
		if(venta.isPresent()){
			return ResponseEntity.ok(venta);
		}else{
			return ResponseEntity.notFound().build();
		}
	}

	
	// En este caso, para las ventas no agregue metodo delete ni actualizar
	// Ya que tomando en cuenta que es facturacion, lo que nos interesa aqui es crear nuevas
	// y obtener registros
	

}
