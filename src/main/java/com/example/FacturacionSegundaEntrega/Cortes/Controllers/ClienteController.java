// Primera Capa
// Primero creamos la clase ClienteController que va a ser la
// encargada de manejar todos los request que reciba la aplicación
// para la ruta /cliente Esta es la primera capa de la aplicación que tiene una instancia
// para llamar a los métodos de la segunda capa.

package com.example.FacturacionSegundaEntrega.Cortes.Controllers;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.FacturacionSegundaEntrega.Cortes.Cliente;
import com.example.FacturacionSegundaEntrega.Cortes.Services.ClienteService;





/* Indica que esta clase es un controlador que maneja solicitudes http (Get, post, etc)
// y retorna objetos tipo JSON*/
@RestController	
// Indica que todas las solicitudes que empiecen con "/cliente" seran manejadas por este controlador
@RequestMapping("/cliente")	
public class ClienteController {
	
	@Autowired
	private ClienteService clienteService;
	
	// http://localhost:8090/cliente
	
	/* Postear un Nuevo Cliente, los datos son recibidos en formato Json con la siguiente estructura:
	 
	{
    	"nombre": "Adrian Junior",
    	"apellido": "Cortes Facio",
   		"rfc": "COFA120401"
	}
	*/
	@PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE})      
	public ResponseEntity<?> guardarCliente(@RequestBody Cliente cliente) {
	

		try {
			Cliente clienteGuardado = clienteService.crearCliente(cliente);
			return ResponseEntity.created(URI.create("")).body(clienteGuardado);
		} 
		catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.internalServerError().body(null);
		}
	}
	
	// Metodo Get para obtener un Cliente mediante su id(rfc), a la url solo agregarle / + no. de rfc
	// si solo pones el "/" obtendras la lista de todos los clientes
	// http://localhost:8090/cliente/
		@GetMapping(value = "/{rfc}", produces = {MediaType.APPLICATION_JSON_VALUE})
		public ResponseEntity<?> getClienteById(@PathVariable(name = "rfc") String rfc) {
			
			Optional<Cliente> cliente = clienteService.buscarClienteById(rfc);
			
			if(cliente.isPresent()){
				return ResponseEntity.ok(cliente);
			}else{
				return ResponseEntity.notFound().build();
			}
		}
		
		
		@GetMapping(value = "/", produces = {MediaType.APPLICATION_JSON_VALUE})
		public ResponseEntity<List<Cliente>> getTodosLosClientes() {
			
			return new ResponseEntity<>(this.clienteService.findAll(), HttpStatus.OK);
		}
	
	
	
	// Editar o Actualizar un cliente ya existente mediante su id(rfc)
	// Los nuevos datos deben enviarse en el mismo formato que para el post
	// http://localhost:8090/cliente/GARA456789
	@PutMapping(value = "/{rfc}", consumes = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<?> update(@RequestBody Cliente cliente, @PathVariable(name = "rfc") String rfc) {    
		
		try {
			Cliente clienteGuardado = clienteService.actualizarCliente(cliente, rfc);
			return ResponseEntity.created(URI.create("")).body(clienteGuardado);
		} 
		catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.internalServerError().body("Hubo un error");
		}
	}
	
	
	
	
	// Para el delete es igual que para el get y update, pones "/" seguido del id del cliente a eliminar
	// http://localhost:8090/cliente/COFA120401
	@DeleteMapping("/{rfc}")
    public String eliminarUsuario(@PathVariable String rfc) {
        try {
	        	clienteService.eliminarUsuarioPorId(rfc);
	        	return "Usuario eliminado con éxito";
        	}
        catch(Exception e) {
        	
        	return "Hubo un error";
        }
        
    }
	
	
	
}