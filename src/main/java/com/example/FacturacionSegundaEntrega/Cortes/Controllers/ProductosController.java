package com.example.FacturacionSegundaEntrega.Cortes.Controllers;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.FacturacionSegundaEntrega.Cortes.Cliente;
import com.example.FacturacionSegundaEntrega.Cortes.Productos;
import com.example.FacturacionSegundaEntrega.Cortes.Services.ProductoService;
import com.example.FacturacionSegundaEntrega.Cortes.Productos;




@RestController	
@RequestMapping("/productos")	
public class ProductosController {
	
	@Autowired
	private ProductoService productoService;
	
	// http://localhost:8089/productos
	
	/* Postear un Nuevo Producto, los datos son recibidos en formato Json con la siguiente estructura:
	 
	   	{
    		"stock": 1,
    		"nombre": "PC",
    		"precio": 3000.99
		}
	*/
	// Crear
	@PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE})      
	public ResponseEntity<?> guardarProducto(@RequestBody Productos producto) {
	

		try {
			Productos productoGuardado = productoService.crearproducto(producto);
			return ResponseEntity.created(URI.create("")).body(productoGuardado);
		} 
		catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.internalServerError().body(null);
		}
	}
	
	
	// Obtener
	// http://localhost:8090/productos/
	@GetMapping(value = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<?> getProductoById(@PathVariable(name = "id") String id) {
		
		Optional<Productos> producto = productoService.buscarProductoById(id);
		
		if(producto.isPresent()){
			return ResponseEntity.ok(producto);
		}else{
			return ResponseEntity.notFound().build();
		}
	}
	
	
	@GetMapping(value = "/", produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<List<Productos>> getTodosLosProductos() {
		
		return new ResponseEntity<>(this.productoService.findAll(), HttpStatus.OK);
	}
	
	
	// Actualizar
	// http://localhost:8090/productos/ID-1704924315
	@PutMapping(value = "/{id}", consumes = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<?> update(@RequestBody Productos productos, @PathVariable(name = "id") String id) {
		
		try {
			Productos productoGuardado = productoService.actualizarproducto(productos, id);
			return ResponseEntity.created(URI.create("")).body(productoGuardado);
		} 
		catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.internalServerError().body(null);
		}
	}
	
	
	// Eliminar
	// http://localhost:8090/productos/ID-1704924315
	@DeleteMapping("/{codigoDeProducto}")
    public String eliminarUsuario(@PathVariable String codigoDeProducto) {
        try {
	        	productoService.eliminarProductoPorId(codigoDeProducto);
	        	return "Producto eliminado con éxito";
        	}
        catch(Exception e) {
        	
        	return "Hubo un error";
        }
        
    }
	

}
