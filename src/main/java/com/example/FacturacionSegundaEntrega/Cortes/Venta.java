package com.example.FacturacionSegundaEntrega.Cortes;

import java.util.Objects;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "Venta")
public class Venta {

	public Venta() {
		super();
	}
	
	@Id
	@Column(name = "Id")
	private String id;
	
	@Column(name = "Cliente")
	private String cliente;
	
	@Column(name = "Cantidad")
	private int cantidad;
	
	@Column(name = "Producto")
	private String producto;
	
	@Column(name = "Total")
	private double total;

	public Venta(String id, String cliente, int cantidad, String producto, double total) {
		super();
		this.id = id;
		this.cliente = cliente;
		this.cantidad = cantidad;
		this.producto = producto;
		this.total = total;
	}

	
	// Metodos Getter & Setter
	

	public void setId(String id) {
		this.id = id;
	}
	
	public String getId() {
		return id;
	}
	
	public String getCliente() {
		return cliente;
	}

	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public String getProducto() {
		return producto;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}


	@Override
	public String toString() {
		return "Venta [Id = " + id + "cliente= " + cliente + ", cantidad= " + cantidad + ", producto= " + producto
				+ ", total= " + total + "]";
	}


	@Override
	public int hashCode() {
		return Objects.hash(cantidad, cliente, producto);
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Venta other = (Venta) obj;
		return cantidad == other.cantidad && Objects.equals(cliente, other.cliente)
				&& Objects.equals(producto, other.producto);
	}
	
	
	
	

}
