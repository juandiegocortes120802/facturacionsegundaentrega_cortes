package com.example.FacturacionSegundaEntrega.Cortes.Services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.FacturacionSegundaEntrega.Cortes.Productos;
import com.example.FacturacionSegundaEntrega.Cortes.Venta;
import com.example.FacturacionSegundaEntrega.Cortes.Repositorios.ProductosRepository;
import com.example.FacturacionSegundaEntrega.Cortes.Repositorios.VentasRepository;



	@Service 
	public class VentaService {

		@Autowired
		private VentasRepository ventasRepository;
		
		public Venta registrarVenta(Venta venta) {
			venta.setId("V" + venta.hashCode());
			return ventasRepository.save(venta);
		}
		
		
		public List<Venta> findAll() {
			
			return ventasRepository.findAll();
		}
		
		public Optional<Venta> buscarVentaById(String id) {
			return ventasRepository.findById(id);
		}
	}
