
// Segunda capa o Capa lógica
// que es la que se comunica con la tercer capa o capa de datos, para
//procesar la información recibida mediante cualquiera de lo multiples metodos CRUD

package com.example.FacturacionSegundaEntrega.Cortes.Services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.FacturacionSegundaEntrega.Cortes.Cliente;
import com.example.FacturacionSegundaEntrega.Cortes.Repositorios.ClienteRepository;

@Service 
public class ClienteService {
		
	@Autowired
	private ClienteRepository clienteRepository;
	
	
	// Crear
	public Cliente crearCliente(Cliente cliente) {
		return clienteRepository.save(cliente);
	}
	
	// Leer
	public Optional<Cliente> buscarClienteById(String rfcCliente) {
		return clienteRepository.findById(rfcCliente);
	}
	
	
	public List<Cliente> findAll() {
		
		return clienteRepository.findAll();
	}
	
	// Actualizar
	public Cliente actualizarCliente(Cliente cliente, String idCliente) {
		Cliente clienteExistente = clienteRepository.findById(idCliente).orElse(null);
		
		if (clienteExistente != null) {
            clienteExistente.setNombre(cliente.getNombre());
            clienteExistente.setApellido(cliente.getApellido());
        }
		return clienteRepository.save(clienteExistente);
	}
	
	// Eliminar
	public void eliminarUsuarioPorId(String rfc) {
        clienteRepository.deleteById(rfc);
    }
}
