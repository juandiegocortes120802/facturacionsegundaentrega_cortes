package com.example.FacturacionSegundaEntrega.Cortes.Services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.FacturacionSegundaEntrega.Cortes.Productos;
import com.example.FacturacionSegundaEntrega.Cortes.Repositorios.ProductosRepository;

@Service 
public class ProductoService {

	@Autowired
	private ProductosRepository productosRepository;
	
	public Productos crearproducto(Productos producto) {
		producto.setId("ID" + producto.hashCode());
		System.out.println(producto);
		return productosRepository.save(producto);
	}
	
	public Productos actualizarproducto(Productos producto, String id) {
		Productos productoExistente = productosRepository.findById(id).orElse(null);
		
		if (productoExistente != null) {
			productoExistente.setNombre(producto.getNombre());
			productoExistente.setPrecio(producto.getPrecio());
			productoExistente.setStock(producto.getStock());
        }
		return productosRepository.save(productoExistente);
	}
	
	public Optional<Productos> buscarProductoById(String id) {
		return productosRepository.findById(id);
	}
	
	public List<Productos> findAll() {
		
		return productosRepository.findAll();
	}
	
	
	public void eliminarProductoPorId(String id) {
        productosRepository.deleteById(id);
    }
}
