package com.example.FacturacionSegundaEntrega.Cortes.Repositorios;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.FacturacionSegundaEntrega.Cortes.Productos;

public interface ProductosRepository extends JpaRepository<Productos, String>{

}
