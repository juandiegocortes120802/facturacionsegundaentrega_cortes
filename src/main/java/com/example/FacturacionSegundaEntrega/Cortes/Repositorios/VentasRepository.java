package com.example.FacturacionSegundaEntrega.Cortes.Repositorios;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.FacturacionSegundaEntrega.Cortes.Venta;



public interface VentasRepository extends JpaRepository<Venta, String>{

}
