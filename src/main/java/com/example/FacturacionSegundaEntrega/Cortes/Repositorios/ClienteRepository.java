// Capa de datos o tercera capa que tiene la
// responsabilidad de conectarse a una base de datos
// Y postear, obtener, actualizar o eliminar en la base de datos

package com.example.FacturacionSegundaEntrega.Cortes.Repositorios;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.FacturacionSegundaEntrega.Cortes.Cliente;


public interface ClienteRepository extends JpaRepository<Cliente,String>
{
}
